import request from '@/utils/request'

// aco
export default {

    test(input) {
        const data = {
            input
        }
        return request({
            url: '/test',
            method: 'post',
            data: data
        })
    },
    acoAlgorithm(matrix, address) {
        const data = {
            matrix,
            address
        }
        return request({
            url: '/algorithm/aco',
            method: 'post',
            data: data
        })
    },
    saAlgorithm(matrix, address) {
        const data = {
            matrix,
            address
        }
        return request({
            url: '/algorithm/sa',
            method: 'post',
            data: data
        })
    },
    gaAlgorithm(matrix, address) {
        const data = {
            matrix,
            address
        }
        return request({
            url: '/algorithm/ga',
            method: 'post',
            data: data
        })
    },
    tabuAlgorithm(matrix, address) {
        const data = {
            matrix,
            address
        }
        return request({
            url: '/algorithm/tabu',
            method: 'post',
            data: data
        })
    },
}
