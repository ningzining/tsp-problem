import { createRouter, createWebHistory } from 'vue-router'
import login from '@/views/login.vue'
import layout from '@/layout/layout.vue'
import NotFound from '@/components/NotFound'


const routes = [
  {
    path: '/',
    name: 'login',
    component: login,
    meta: {
      title: "登录",
    }
  },
  {
    path: '/index',
    name: '首页1',
    component: layout,
    redirect: '/index/1-1',
    children: [
      {
        path: '1-1',
        component: () => import("@/views/tsp"),
        name: 'tsp解决方案',
        meta: {
          title: 'tsp解决方案',
        }
      },
      {
        path: '1-2',
        component: () => import("@/views/tspfile"),
        name: 'tsp文件输入',
        meta: {
          title: 'tsp文件输入',
        }
      }
    ]
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound', 
    component: NotFound,
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
