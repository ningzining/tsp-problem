import { createStore } from 'vuex'
import user from './modules/user'
import map from './modules/map'

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    user,
    map,
  }
})
