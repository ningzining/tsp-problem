const map = {
    state: {
        gpsArray: [],
        address: '',
    },
    actions: {
        saveGps({ commit }, msg) {
            console.log(msg)
            commit('saveGpsArray', msg)    // 提交到mutations中处理    
        },
        saveAdd({ commit }, msg) {
            commit('saveAddress', msg)    // 提交到mutations中处理    
        }
    },
    mutations: {
        saveGpsArray: (state, msg) => {
            state.gpsArray = msg
        },
        saveAddress: (state, msg) => {
            state.address = msg
        },
    },
}
export default map