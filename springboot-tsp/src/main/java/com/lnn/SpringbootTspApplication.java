package com.lnn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootTspApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootTspApplication.class, args);
    }

}
