package com.lnn.controller;

import com.lnn.algorithm.aco.ACO;
import com.lnn.algorithm.ga.GA;
import com.lnn.algorithm.sa.SA;
import com.lnn.algorithm.tabu.Tabu;
import com.lnn.pojo.Route;
import com.lnn.pojo.dto.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author: Li Ning Ning
 * @time: 2021/7/24 10:06
 * @description: TODO
 * @version: 1.0
 */
@RestController
@RequestMapping("file")
public class FileController {

    @ApiOperation(value = "文件输入蚁群算法求解")
    @PostMapping("/algorithm/aco")
    public Result fileAco(MultipartFile file) throws IOException {
        ACO aco = new ACO(50, 500, 1.0f, 5.0f, 0.5f, 1.0f);
        ArrayList<Route> solve = aco.init(file).solve();
        return Result.success().data(solve);
    }

    @ApiOperation(value = "文件输入模拟退火算法求解")
    @PostMapping("/algorithm/sa")
    public Result fileSa(MultipartFile file) throws IOException {
        SA sa = new SA(100, 1000.0f, 0.01f, 0.98f);
        ArrayList<Route> solve = sa.init(file).solve();
        return Result.success().data(solve);
    }

    @ApiOperation(value = "文件输入遗传算法求解")
    @PostMapping("/algorithm/ga")
    public Result fileGa(MultipartFile file) throws IOException {
        GA ga = new GA(50, 10000, 0.9f, 0.9f);
        ArrayList<Route> solve = ga.init(file).solve();
        return Result.success().data(solve);
    }

    @ApiOperation(value = "文件输入禁忌搜索算法求解")
    @PostMapping("/algorithm/tabu")
    public Result fileTabu(MultipartFile file) throws IOException {
        Tabu tabu = new Tabu(5000, 200, 40);
        ArrayList<Route> solve = tabu.init(file).solve();
        return Result.success().data(solve);
    }
}
