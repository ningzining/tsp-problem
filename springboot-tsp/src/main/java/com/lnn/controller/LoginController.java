package com.lnn.controller;

import com.lnn.pojo.User;
import com.lnn.pojo.dto.Result;
import com.lnn.util.JWTUtils;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author: Li Ning Ning
 * @time: 2021/7/12 19:18
 * @description: TODO
 * @version: 1.0
 */
@Slf4j
@RestController
public class LoginController {

    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public Result hello(@RequestBody User user) {
        log.info("登录用户为：" + user);
        if (user == null) {
            return Result.error();
        }
        return Result.success(JWTUtils.createToken(user.getUsername()));
    }

}
