package com.lnn.controller;

import com.lnn.algorithm.aco.ACO;
import com.lnn.algorithm.ga.GA;
import com.lnn.algorithm.sa.SA;
import com.lnn.algorithm.tabu.Tabu;
import com.lnn.pojo.Route;
import com.lnn.pojo.dto.JsonObject;
import com.lnn.pojo.dto.Result;
import com.lnn.util.Address;
import com.lnn.util.Matrix;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @author: Li Ning Ning
 * @time: 2021/7/23 16:14
 * @description: TODO
 * @version: 1.0
 */
@RestController
@RequestMapping("algorithm")
public class AlgorithmController {

    @ApiOperation(value = "蚁群算法求解")
    @PostMapping("/aco")
    public Result aco(@RequestBody JsonObject jsonObject) {
        int[][] matrix = Matrix.getMatrixByString(jsonObject.getMatrix());
        String[] address = Address.getAddressByString(jsonObject.getAddress());
        if (matrix.length != address.length) {
            return Result.error();
        }
        ACO aco = new ACO(matrix.length, 50, 500, 1f, 5.0f, 0.5f, 1.0f);
        ArrayList<Route> solve = aco.init(matrix, address).solve();
        return Result.success().data(solve);
    }

    @ApiOperation(value = "模拟退火算法求解")
    @PostMapping("/sa")
    public Result sa(@RequestBody JsonObject jsonObject) {
        int[][] matrix = Matrix.getMatrixByString(jsonObject.getMatrix());
        String[] address = Address.getAddressByString(jsonObject.getAddress());
        if (matrix.length != address.length) {
            return Result.error();
        }
        SA sa = new SA(matrix.length, 100, 1000.0f, 0.01f, 0.98f);
        ArrayList<Route> solve = sa.init(matrix, address).solve();
        return Result.success().data(solve);
    }

    @ApiOperation(value = "遗传算法求解")
    @PostMapping("/ga")
    public Result ga(@RequestBody JsonObject jsonObject) {
        int[][] matrix = Matrix.getMatrixByString(jsonObject.getMatrix());
        String[] address = Address.getAddressByString(jsonObject.getAddress());
        if (matrix.length != address.length) {
            return Result.error();
        }
        GA ga = new GA(matrix.length, 50, 10000, 0.9f, 0.9f);
        ArrayList<Route> solve = ga.init(matrix, address).solve();
        return Result.success().data(solve);
    }

    @ApiOperation(value = "禁忌搜索算法求解")
    @PostMapping("/tabu")
    public Result tabu(@RequestBody JsonObject jsonObject) {
        int[][] matrix = Matrix.getMatrixByString(jsonObject.getMatrix());
        String[] address = Address.getAddressByString(jsonObject.getAddress());
        if (matrix.length != address.length) {
            return Result.error();
        }
        Tabu tabu = new Tabu(matrix.length, 5000, 200, 40);
        ArrayList<Route> solve = tabu.init(matrix, address).solve();
        return Result.success().data(solve);
    }
}
