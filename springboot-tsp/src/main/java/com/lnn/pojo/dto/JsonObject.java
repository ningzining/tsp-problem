package com.lnn.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author: Li Ning Ning
 * @time: 2021/7/23 16:13
 * @description: TODO
 * @version: 1.0
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel(value = "json对象")
public class JsonObject {

    @ApiModelProperty(value = "距离矩阵")
    private String matrix;

    @ApiModelProperty(value = "地址名字")
    private String address;

}
