package com.lnn.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Li Ning Ning
 * @time: 2021/7/12 19:19
 * @description: TODO
 * @version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "返回类")
public class Result {

    @ApiModelProperty(value = "响应码")
    private Integer code;

    @ApiModelProperty(value = "响应信息")
    private String message;

    @ApiModelProperty(value = "响应数据")
    private Object data;

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static Result success() {
        return new Result(ResultStatus.SUCCESS.code(), ResultStatus.SUCCESS.message());
    }

    public static Result success(Object data) {
        return new Result(ResultStatus.SUCCESS.code(), ResultStatus.SUCCESS.message(), data);
    }

    public static Result success(String message, Object data) {
        return new Result(ResultStatus.SUCCESS.code(), message, data);
    }

    public static Result error() {
        return new Result(ResultStatus.ERROR.code(), ResultStatus.ERROR.message());
    }

    public static Result error(String message) {
        return new Result(ResultStatus.ERROR.code(), message);
    }

    public Result message(String message) {
        this.message = message;
        return this;
    }

    public Result data(Object data) {
        this.data = data;
        return this;
    }
}
