package com.lnn.pojo.dto;

/**
 * @author: Li Ning Ning
 * @time: 2021/7/14 18:24
 * @description: TODO
 * @version: 1.0
 */
public enum ResultStatus {

    SUCCESS(200, "请求成功"),
    ERROR(500, "请求失败");

    private final Integer code;

    private final String message;

    ResultStatus(Integer code, String msg) {
        this.code = code;
        this.message = msg;
    }

    public Integer code() {
        return code;
    }

    public String message() {
        return message;
    }
}
