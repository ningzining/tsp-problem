package com.lnn.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author: Li Ning Ning
 * @time: 2021/7/24 15:23
 * @description: TODO
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Node {

    private int x;
    private int y;

}
