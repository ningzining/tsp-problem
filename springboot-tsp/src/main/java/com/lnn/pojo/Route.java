package com.lnn.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author: Li Ning Ning
 * @time: 2021/7/23 16:13
 * @description: TODO
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel(value = "路线类")
public class Route {

    @ApiModelProperty(value = "城市编号")
    private int number;

    @ApiModelProperty(value = "城市坐标")
    private Node node;

    @ApiModelProperty(value = "城市地址")
    private String address;

    @ApiModelProperty(value = "与下一个城市的距离")
    private int distance;

    @ApiModelProperty(value = "总城市数")
    private int totalCity;

    @ApiModelProperty(value = "总距离")
    private int totalDistance;

}
