package com.lnn.util;

/**
 * @author: Li Ning Ning
 * @time: 2021/7/23 16:15
 * @description: TODO
 * @version: 1.0
 */
public class Matrix {

    /**
     * 将矩阵字符串转化为二维数组
     *
     * @param input
     * @return
     */
    public static int[][] getMatrixByString(String input) {
        String[] split = input.split("\n");
        int length = Integer.parseInt(split[0]);
        int[][] matrix = new int[length][length];
        for (int i = 0; i < length; i++) {
            String[] str = split[i + 1].split(" ");
            for (int j = 0; j < length; j++) {
                matrix[i][j] = Integer.parseInt(str[j]);
            }
        }
        return matrix;
    }

    /**
     * 计算距离矩阵
     *
     * @param x
     * @param y
     */
    public static int[][] computeDistanceMatrix(int[] x, int[] y) {
        int[][] distance = new int[x.length][y.length];
        for (int i = 0; i < x.length - 1; i++) {
            distance[i][i] = 0; // 对角线为0
            for (int j = i + 1; j < x.length; j++) {
                double rij = Math
                        .sqrt((x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j])
                                * (y[i] - y[j]));
                int tij = (int) Math.round(rij);
                if (tij < rij) {
                    distance[i][j] = tij + 1;
                } else {
                    distance[i][j] = tij;
                }
                distance[j][i] = distance[i][j];
            }
        }
        distance[x.length - 1][x.length - 1] = 0;
        return distance;
    }

}
