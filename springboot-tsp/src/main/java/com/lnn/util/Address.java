package com.lnn.util;

/**
 * @author: Li Ning Ning
 * @time: 2021/7/23 16:16
 * @description: TODO
 * @version: 1.0
 */
public class Address {

    /**
     * 将字符串地址转化为地址数组
     *
     * @param address
     * @return
     */
    public static String[] getAddressByString(String address) {
        return address.split("\n");
    }

    /**
     * 将数组排序成从第一个城市开始
     *
     * @param array
     * @return
     */
    public static int[] StartAtZero(int[] array) {
        int start = array[0];
        int[] res = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                start = i;
                break;
            }
        }
        for (int i = 0; i < array.length; i++) {
            if (start == array.length - 1) {
                start = 0;
            }
            res[i] = array[start];
            start++;
        }
        return res;
    }

    /**
     * 将城市编号转为为对应的城市名字
     *
     * @param cities
     * @param address
     * @return
     */
    public static String[] arrayToAddress(int[] cities, String[] address) {
        String[] res = new String[cities.length];
        for (int i = 0; i < cities.length; i++) {
            res[i] = address[cities[i]];
        }
        return res;
    }
}
