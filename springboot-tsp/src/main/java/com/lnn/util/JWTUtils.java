package com.lnn.util;

import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: Li Ning Ning
 * @since: 2022/2/24 20:51
 * @version: 1.0
 */
public class JWTUtils {
    // 定义加密的盐
    private static final String jwtToken = "771846387@qq.com";
    // 定义签发者
    private static final String ISSUER = "entangledCotton";
    //1000*60*60*24 24小时
    private static final Long TOKEN_EXPIRE_TIME = 1000 * 60 * 60 * 24L;

    /**
     * 创建token
     * @param userName 用户名
     * @return token
     */
    public static String createToken(String userName){
        Map<String,Object> claims = new HashMap<>();
        claims.put("userId",userName);
        JwtBuilder jwtBuilder = Jwts.builder()
                .setIssuer(ISSUER) // 签发者
                .signWith(SignatureAlgorithm.HS256, jwtToken) // 签发算法，秘钥为jwtToken
                .setClaims(claims) // body数据，要唯一，自行设置
                .setIssuedAt(new Date()) // 设置签发时间
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_EXPIRE_TIME)); // 一天的有效时间
        String token = jwtBuilder.compact();
        return token;
    }

    /**
     * 验证token
     * @param token token
     * @return body数据
     */
    public static Map<String, Object> checkToken(String token){
        try {
            Jwt parse = Jwts.parser().setSigningKey(jwtToken).parse(token);
            return (Map<String, Object>) parse.getBody();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
