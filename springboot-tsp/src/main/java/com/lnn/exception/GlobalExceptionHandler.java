package com.lnn.exception;

import com.lnn.pojo.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

/**
 * 全局异常捕获
 *
 * @author: Li Ning Ning
 * @time: 2021/7/20 14:59
 * @description: TODO
 * @version: 1.0
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 捕获全局异常
     */
    @ExceptionHandler(Exception.class)
    public Result handleException(Exception e) {
        log.info("出现未知错误 -> " + e);
        return Result.error().message(e.getMessage());
    }

    /**
     * 捕获空指针异常
     */
    @ExceptionHandler(NullPointerException.class)
    public Result handleNullPointerException(Exception e) {
        log.info("空指针异常 -> " + e);
        return Result.error().message("空指针异常");
    }

    /**
     * 捕获文件异常
     */
    @ExceptionHandler(IOException.class)
    public Result handleIOException(Exception e) {
        log.info("文件输入异常 -> " + e);
        return Result.error().message("文件输入异常");
    }
}
